import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { Router } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  loginGroup = new FormGroup({
    username: new FormControl(null, [Validators.required]),
    password: new FormControl(null, [Validators.required])
  });

  loginFormError: string;

  constructor(private readonly authenticationService: AuthenticationService, private router: Router) {
  }

  ngOnInit() {
    if (this.authenticationService.hasTokken()) {
      this.router.navigate(["/home"]);
    }
  }

  login() {
    delete this.loginFormError;
    if (this.loginGroup.invalid) {
      this.loginFormError = 'tous les champs ne sont pas valides.';
      return;
    }

    let username: string = this.loginGroup.get("username").value;
    let password: string = this.loginGroup.get("password").value;

    this.authenticationService.login(username, password).subscribe({
      next: response => {
        localStorage.setItem('username', response.username);
        localStorage.setItem('token', response.token);
        this.router.navigate(["/home"]);
      },
      error: err => {
        console.log(err);
        this.loginFormError = err.statusText;
      }
    });
  }

}
