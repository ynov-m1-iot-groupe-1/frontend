import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/models/card.model';
import { SocketService } from 'src/app/services/socket.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  constructor(private socketService: SocketService) { }

  card: Card;
  tempChart: { name: "Température", series: { name: string, value: number }[] }[] = [{ name: "Température", series: [] }];
  humChart: { name: "Humidité", series: { name: string, value: number }[] }[] = [{ name: "Humidité", series: [] }];
  sensoilChart: { name: "Humidité du sol", series: { name: string, value: number }[] }[] = [{ name: "Humidité du sol", series: [] }];

  tempSpliceIndex = 1;
  humSpliceIndex = 1;
  sensoilSpliceIndex = 1;

  tempLimit: { min: number, max: number } = { min: 25, max: 25 };

  private socketConnection;

  ngOnInit() {
    this.socketConnection = this.socketService.getCard().subscribe({
      next: (response: Card) => {
        response.modificationDate = new Date(response.modificationDate);
        if (response.temp < this.tempLimit.min) this.tempLimit.min = +response.temp;
        if (response.temp > this.tempLimit.max) this.tempLimit.max = +response.temp;
        this.pushOldValue(response);
        this.card = { ...response };
      }
    });
  }

  spliceOdd(array, index): number {
    if (array.length <= index) {
      index = 1;
    }
    array.splice(index, 1);
    return index + 2;
  }

  pushOldValue(card: Card) {
    if (this.tempChart[0].series.length > 10) this.tempSpliceIndex = this.spliceOdd(this.tempChart[0].series, this.tempSpliceIndex);
    if (this.humChart[0].series.length > 10) this.humSpliceIndex = this.spliceOdd(this.humChart[0].series, this.humSpliceIndex);
    if (this.sensoilChart[0].series.length > 10) this.sensoilSpliceIndex = this.spliceOdd(this.sensoilChart[0].series, this.sensoilSpliceIndex);
    if (this.card) {
      if (card.temp != this.card.temp) {
        this.tempChart[0].series.push({ name: this.timeToString(card.modificationDate), value: card.temp });
        this.tempChart = [...this.tempChart];
      }
      if (card.hum != this.card.hum) {
        this.humChart[0].series.push({ name: this.timeToString(card.modificationDate), value: card.hum });
        this.humChart = [...this.humChart];
      }
      if (card.sensoil != this.card.sensoil) {
        this.sensoilChart[0].series.push({ name: this.timeToString(card.modificationDate), value: card.sensoil });
        this.sensoilChart = [...this.sensoilChart];
      }
    } else {
      this.tempChart[0].series.push({ name: this.timeToString(card.modificationDate), value: card.temp });
      this.humChart[0].series.push({ name: this.timeToString(card.modificationDate), value: card.hum });
      this.sensoilChart[0].series.push({ name: this.timeToString(card.modificationDate), value: card.sensoil });
      this.tempLimit = { min: card.temp, max: card.temp }
    }
  }

  appendZero(string: string) {
    return string.length > 1 ? string : "0" + string;
  }

  timeToString(date: Date): string {
    return `${this.appendZero(date.getHours().toString())}:${this.appendZero(date.getMinutes().toString())}:${this.appendZero(date.getSeconds().toString())}`;
  }

  dateToString(date: Date): string {
    return `${this.appendZero(date.getDate().toString())}/${this.appendZero((date.getMonth() + 1).toString())}/${date.getFullYear()}`;
  }

  public toggleLed() {
    this.socketService.sendToggleLedMessage(!this.card.led);
  }

  /**
  * Unsubscribe socket connection
  */
  public ngOnDestroy() {
    if (this.socketConnection) {
      this.socketConnection.unsubscribe();
    }
  }


  // CHART

  tempChartParams = {
    view: [500, 300],
    legend: false,
    legendPosition: 'below',
    showLabels: true,
    animations: true,
    xAxis: true,
    yAxis: true,
    showYAxisLabel: true,
    showXAxisLabel: true,
    xAxisLabel: '',
    yAxisLabel: 'Température ( °C )',
    timeline: true,
    colorScheme: {
      domain: ['#E44D25']
    },
    yScaleMin: (this.tempLimit.min - 1),
    yScaleMax: (this.tempLimit.max + 1),
    activeEntries: this.tempChart[0].series
  }

  humChartParams = {
    view: [500, 300],
    // options
    legend: false,
    legendPosition: 'below',
    showLabels: true,
    animations: true,
    xAxis: true,
    yAxis: true,
    showYAxisLabel: true,
    showXAxisLabel: true,
    xAxisLabel: '',
    yAxisLabel: 'Humidité ( % )',
    timeline: true,
    colorScheme: {
      domain: ['#31b4c2']
    },
    yScaleMin: 0,
    yScaleMax: 100
  }

  sensoilChartParams = {
    view: [500, 300],
    // options
    legend: false,
    legendPosition: 'below',
    showLabels: true,
    animations: true,
    xAxis: true,
    yAxis: true,
    showYAxisLabel: true,
    showXAxisLabel: true,
    xAxisLabel: '',
    yAxisLabel: 'Humidité du sol ( % )',
    timeline: true,
    colorScheme: {
      domain: ['#5AA454']
    },
    yScaleMin: 0,
    yScaleMax: 100
  }
}