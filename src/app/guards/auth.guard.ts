import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

/** Guards to check the connexion of the user */
@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(private authenticationService: AuthenticationService, private router: Router) { }

    private async isLogged(): Promise<boolean> {
        if (this.authenticationService.hasTokken()) {
            return this.authenticationService.checkToken().toPromise().then(
                (response) => {
                    return true;

                },
                (err) => {
                    console.log(err);
                    this.router.navigate(["/login"]);
                    return false;
                });
        } else {
            this.router.navigate(["/login"]);
        }
    }
    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.isLogged();
    }
}