export class Card {
    name: string;
    modificationDate: Date;
    temp: number;
    hum: number;
    sensoil: number;
    led: boolean;
}