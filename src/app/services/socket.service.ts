import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  public static socket: SocketIOClient.Socket;

  private CARD = 'CARD';
  private LED = 'LED';

  constructor() {
    SocketService.socket = SocketService.socket ? SocketService.socket : io.connect(environment.BACKEND_URL);
  }

  /**
   * Get card updates
   */
  public getCard() {

    return new Observable(observer => {
      SocketService.socket.on(this.CARD, (response) => {
        observer.next(response);
      });
    });
  }

  public sendToggleLedMessage(led: boolean) {
    SocketService.socket.emit(this.LED, led);
  }

}