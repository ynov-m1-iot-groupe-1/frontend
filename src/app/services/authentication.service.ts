import { Injectable } from "@angular/core";
import {
    HttpClient,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from 'src/environments/environment';
@Injectable({
    providedIn: "root"
})
export class AuthenticationService {
    url = environment.BACKEND_URL + "/auth/";

    constructor(private http: HttpClient) { }

    public login(username: string, password: string): Observable<any> {
        return this.http.post(this.url + "login", { username, password });
    }

    public checkToken(): Observable<any> {
        return this.http.get(this.url + "check-token", { headers: { authorization: localStorage.getItem('token') } });
    }

    hasTokken() {
        if (localStorage.getItem('token')) {
            return true;
        }
        return false;
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('username');
    }
}
